import scrapy
import requests
import pandas


class Flat:
    flats = []

    def __init__(self, price, numOfRooms, space, floor):
        self.price = price
        self.numOfRooms = numOfRooms
        self.space = space
        self.floor = floor


page = 1
while len(Flat.flats) < 200:
    url = f"https://place.ge/ge/ads/page:{page}?type=for_sale&object_type=flat&order_by=date&currency_id=1&city_id=1&mode=list&limit=15"

    response = requests.get(url)
    parsedResponse = scrapy.http.TextResponse(response.url, body=response.text, encoding='utf-8')
    listingBlocks = parsedResponse.css("div.infoFilter").getall()

    for listing in listingBlocks:
        parsedListing = scrapy.http.TextResponse("blank", body=listing, encoding="utf-8")
        price = parsedListing.css('span strong::text').get() 

        splitted = listing.split(",")
        numOfRooms = splitted[2] 
        space = splitted[3] 
        floor = splitted[4] 

        flat = Flat(price, numOfRooms, space, floor )
        Flat.flats.append(flat)


    page += 1


flatDfData = {
    "price": [],
    "rooms": [],
    "space": [],
    "floor": [],
}

for flat in Flat.flats:
    flatDfData['price'].append(flat.price)
    flatDfData['rooms'].append(flat.numOfRooms)
    flatDfData['space'].append(flat.space)
    flatDfData['floor'].append(flat.floor)

df = pandas.DataFrame(flatDfData)
df.to_excel('flats.xlsx', 'flats')




