numbs = [1,2,3,4,5]


minimum = min(numbs)
maximum = max(numbs)
mean = sum(numbs) / len(numbs)


numbs.append(10)
numbs.insert(2, 99)
numbs.pop(3)

numbs.sort()

print(numbs)