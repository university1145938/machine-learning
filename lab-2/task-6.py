def deleteOdds(nums):
    # i = 0
    # while i < len(nums):
    #     if nums[i] % 2 == 1:
    #         nums.pop(i)
    #     else:
    #         i += 1

    # more efficient approach
    onlyEvens = []
    for elem in nums:
        if elem % 2 == 0: onlyEvens.append(elem)

    return onlyEvens


nums = [1,2,3,4,5,6,7]
evens = deleteOdds(nums)
print(evens)
print(nums)
