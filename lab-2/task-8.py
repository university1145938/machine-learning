# იპოვეთ სიაში [1, 5, 23, 5, 12, 2, 5, 1, 18, 5] ყველაზე ხშირად განმეორებადი რიცხვი.
# დაბეჭდეთ შედეგი. ასევე მიუთითეთ რამდენჯერ შეგხვდათ სიაში ყველაზე
# ხშირად განმეორებადი რიცხვი.

import collections
import heapq


nums = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]


elemCounts = collections.Counter(nums)


heap = []
for elem in elemCounts:
    heap.append(
        (elemCounts[elem], elem)
    )

heapq.heapify(heap)

# python by default provides min heap, so if we pop elements from the heap, until there are only 3 elems
# we will end up with top 3 elements

while len(heap) > 3:
    heapq.heappop(heap)

print(heap)

