import pandas as pd
import os

if os.path.exists('data/data.xlsx'):

    sheetOne = pd.read_excel('data/data.xlsx', sheet_name="sheetOne")
    
    # way 1
    # matchedRows = {
    #     "col1": [],
    #     "col2": [],
    #     "col3": [],
    # }
    
    # for i in range(len(sheetOne)):
    #     if 'a' in sheetOne['col1'][i]:
    #         matchedRows['col1'].append(sheetOne['col1'][i])
    #         matchedRows['col2'].append(sheetOne['col2'][i])
    #         matchedRows['col3'].append(sheetOne['col3'][i])
    
    # matchedDf = pd.DataFrame(matchedRows)

    # way 2
    def contains_a(value):
        if pd.isna(value):  # This checks for NaN and returns False if the value is NaN
            return False
        return 'a' in value

    filteredDf = sheetOne[sheetOne['col1'].apply(contains_a)]


    filePath = 'data/datanew.xlsx';
    sheetMode = None
    mode = "w"
    
    if os.path.exists(filePath):
        sheetMode = 'replace'
        mode = 'a'
    
    with pd.ExcelWriter(filePath ,engine='openpyxl', mode=mode, if_sheet_exists=sheetMode) as writer:
        filteredDf.to_excel(writer, sheet_name="sheet3")
    
        

