from string import ascii_lowercase 
from random import randint

def randomString(length):
    result = []
    for i in range(length):
        randIdx = randint(0, len(ascii_lowercase) - 1)
        result.append(
            ascii_lowercase[randIdx]
        )
    
    return "".join(result)

def randomDistinctNumbers(start, end, n):
    if n - 1 > end - start:
        raise Exception('n must be less or equal to the provided range')


    numbers = set([])

    for i in range(n):
        randNum = randint(start, end)
        while randNum in numbers:
            randNum = randint(1, 100)
        
        numbers.add(randNum)
    
    return list(numbers)