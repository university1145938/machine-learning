import pandas as pd
from random import randint
from helpers import randomDistinctNumbers
import os


data = {
    'col1': [],
    'col2': [],
    'col3': [],
    'col4': [],
}

FIRST_NAMES = ['dave', 'john', 'jane', 'mike']
LAST_NAMES = ['marley', 'brown', 'white', 'chopin']

for i in range(50):
    data['col2'].append(
        FIRST_NAMES[randint(0, len(FIRST_NAMES) - 1)]
    )

    data['col3'].append(
        LAST_NAMES[randint(0, len(LAST_NAMES) - 1)]
    )

    data['col4'].append(
        randint(2000, 5000)
    )

data['col1'] = randomDistinctNumbers(1, 100, 50)

df = pd.DataFrame(data)

if not os.path.isdir('data'):
    os.mkdir('data')



filename = 'data/data.xlsx'
sheetName = 'sheetTwo'
fileWriterMode = 'w'
sheetMode = None

if os.path.exists('data/data.xlsx'):
    fileWriterMode = 'a'
    sheetMode = 'replace'

with pd.ExcelWriter(filename, mode=fileWriterMode, engine='openpyxl', if_sheet_exists=sheetMode) as writer:
    df.to_excel(writer, sheet_name=sheetName, index=False)