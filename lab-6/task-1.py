from helpers import randomString 
from helpers import randomDistinctNumbers
from random import randint
import pandas as pd
import os


data = {
    'col1': [],
    'col2': [],
    'col3': [],
    'col4': [],
}

for i in range(100):
    data['col1'].append(randomString(10))
    data['col2'].append(randint(0, 10))
    data['col3'].append(randint(1, 7))

data['col4'] = randomDistinctNumbers(1, 100, 100)

df = pd.DataFrame(data)

if not os.path.isdir('data'):
    os.mkdir('data')


filename = 'data/data.xlsx'
sheetName = 'sheetOne'
fileWriterMode = 'w'
sheetMode = None

if os.path.exists('data/data.xlsx'):
    fileWriterMode = 'a'
    sheetMode = 'replace'

with pd.ExcelWriter(filename, mode=fileWriterMode, engine='openpyxl', if_sheet_exists=sheetMode) as writer:
    df.to_excel(writer, sheet_name=sheetName, index=False)


