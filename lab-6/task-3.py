import pandas as pd
import os


if os.path.isfile('data/data.xlsx'):
    mode = "w"
    sheetMode = None
    filePath = 'data/datanew.xlsx'

    if os.path.exists(filePath):
        mode = "a"
        sheetMode = 'replace'
    
    fileContents = pd.read_excel('data/data.xlsx', sheet_name=None)
    
    for sheetName in fileContents:
        with pd.ExcelWriter(path=filePath, engine='openpyxl', if_sheet_exists=sheetMode, mode=mode) as writer:
            fileContents[sheetName].to_excel(writer)



