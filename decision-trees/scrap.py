import scrapy
import requests
import pandas
import json

class Flat:
    flats = []

    def __init__(self, price, numOfRooms, space, floor, totalFloors):
        self.price = price
        self.numOfRooms = numOfRooms
        self.space = space
        self.floor = floor
        self.totalFloors = totalFloors
    
    def __repr__(self):
        return f"price: {self.price}, rooms: {self.numOfRooms}, space: {self.space}, floor: {self.floor}, total floors: {self.totalFloors} \n"


numOfListings = 200
url = f"https://place.ge/ge/ads/page:1?type=for_sale&object_type=flat&currency_id=1&city_id=1&region_id=1&mode=list&order_by=date&limit={numOfListings}" 

response = requests.get(url)
parsedResponse = scrapy.http.TextResponse(response.url, body=response.text, encoding='utf-8')
listingBlocks = parsedResponse.css("div.infoFilter").getall()

for listing in listingBlocks:
    parsedListing = scrapy.http.TextResponse("blank", body=listing, encoding="utf-8")
    price = parsedListing.css('span strong::text').get() 
    priceInGel = price.strip().split(" ")[0] 

    splitted = listing.strip().split(",")
    numOfRooms = splitted[2].strip().split(' ')[0]
    space = splitted[3].strip().split(' ')[0]
    floors = splitted[4].strip().split(' ')[1]
    floor = floors.split('/')[0]
    totalFloors = floors.split('/')[1].split('<br>')[0]

    flat = Flat(priceInGel, numOfRooms, space, floor, totalFloors)
    Flat.flats.append(flat)


trainingData = {
    "price": [],
    "rooms": [],
    "space": [],
    "floor": [],
    "total_floors": [],
}

testingData = {
    "price": [],
    "rooms": [],
    "space": [],
    "floor": [],
    "total_floors": [],
}

for idx, flat in enumerate(Flat.flats):
    if idx < 150:
        trainingData['price'].append(flat.price)
        trainingData['rooms'].append(flat.numOfRooms)
        trainingData['space'].append(flat.space)
        trainingData['floor'].append(flat.floor)
        trainingData['total_floors'].append(flat.totalFloors)
    else:
        testingData['price'].append(flat.price)
        testingData['rooms'].append(flat.numOfRooms)
        testingData['space'].append(flat.space)
        testingData['floor'].append(flat.floor)
        testingData['total_floors'].append(flat.totalFloors)


trainingDf = pandas.DataFrame(trainingData)
trainingDf.to_excel('training_data.xlsx', 'flats')

testingDf = pandas.DataFrame(testingData)
testingDf.to_excel('testing_data.xlsx', 'flats')



