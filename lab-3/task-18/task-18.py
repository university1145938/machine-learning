numsTuple = tuple(
    num ** 3 for num in range(100) if num % 5 == 0
)


print(numsTuple)
print(len(numsTuple))

