set1 = set(['green', 'blue'])
set2 = set(['yellow', 'blue'])


print( f"union {set1 | set2}" )
print(f"intersection {set1 & set2}")
print(f"difference {set1 - set2}")
print(f"symmetric difference{set1 ^ set2}")
print("\n")




# union by hand
unionSet = set([])

for setObj in [set1, set2]:
    for elem in setObj:
        unionSet.add(elem)

print(f"union set by hand {unionSet}") 

# intersection by hand
intersection = []

for elem in set1:
    if elem in set2:
        intersection.append(elem)


print(f"intersection by hand {intersection}") 


# difference by hand
difference = []

for elem in set1:
    if elem not in set2:
        difference.append(elem)

print(f"difference by hand {difference}") 




# symmetric difference by hand
symmetricDifference = []

for elem in set1:
    if elem not in set2:
        symmetricDifference.append(elem)

for elem in set2:
    if elem not in set1:
        symmetricDifference.append(elem)


print(f"symmetric difference by hand {symmetricDifference}") 