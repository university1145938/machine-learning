import os
import pandas as pd

data = {
    "col1": ["a", "b", "c"],
    "col2": ["d", "e", "f"],
}

df = pd.DataFrame(data)

# simple way
# cant append to existing file this way
df.to_excel("./write-excel.xlsx", sheet_name="sheet_2")


# more complex way, but can do appends
# if sheet already exists and we want to write in append mode
# we should pass mode="a" and if_sheet_exist="replace" params

with pd.ExcelWriter("write-excel.xlsx", engine='openpyxl', mode="w") as writer:
    df.to_excel(writer, sheet_name="new_sheet_3")

