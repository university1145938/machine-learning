import pandas as pd
import matplotlib.pyplot as plt


data = {
    "price": [10_000, 20_000, 15_000, 25_000, 25_000],
    "space": [100000, 50000, 6000, 8000],
    "flor": [4, 2, 7, 3],
}

df = pd.DataFrame(data)


# scatter plot
df.plot(
    kind="scatter", x="flor", y="price"
)

# histogram
df['price'].plot(
    kind="hist"
)

plt.show()