from sklearn.linear_model import LinearRegression

model = LinearRegression()

x = [ [1, 30], [2, 70], [3, 90]  ]   # rooms, space
y = [ [15_000], [35_000], [45_000] ]   # prices

model.fit(x, y)


result = model.predict(
    [[5, 150]]
)

print(result)