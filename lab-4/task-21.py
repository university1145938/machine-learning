import numpy

matrix = []

for i in range(10):
    row = []
    for i in range(10):
        row.append(numpy.random.randint(0,11))
    matrix.append(row)

matrix = numpy.array(matrix)
print(matrix)
print("\n")
matrixWithDeletedRow = numpy.delete(matrix, 9, 0)
print(matrixWithDeletedRow)
