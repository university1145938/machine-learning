import numpy


def generateMatrix():
    matrix = []
    for i in range(10):
        row = []
        for j in range(10):
            row.append(numpy.random.randint(0,11))
        matrix.append(row)
    
    return matrix

matrix1 = generateMatrix()
matrix2 = generateMatrix()


matrix1 = numpy.array(matrix1)
matrix2 = numpy.array(matrix2)

concatenated = numpy.concatenate((matrix1.flatten(), matrix2.flatten())) 

print(concatenated)
