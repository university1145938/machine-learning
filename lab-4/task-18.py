import numpy


matrix1= [
    [1,2],
    [3,4],
    [1,1],
]

matrix2= [
    [1,2,3],
    [2,3,4],
]

print(numpy.matmul(matrix1, matrix2))