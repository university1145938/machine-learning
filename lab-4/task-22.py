import numpy

matrix = []

for i in range(10):
    row = []
    for i in range(10):
        row.append(numpy.random.randint(0,11))
    matrix.append(row)

matrix = numpy.array(matrix)
print(matrix)
print("\n")


matrix[matrix == 0] = 1
print(matrix)
