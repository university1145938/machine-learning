import random
import collections

def createNList(n):
    result = []
    for i in range(n):
        result.append(random.randint(5, 15))
    return result

arr = createNList(20)
mapWithElemCounts = collections.Counter(arr)

elemWithMaxRepetitions = None
maxRepetitionsSoFar = 0

print(mapWithElemCounts)

for elem in mapWithElemCounts:
    if mapWithElemCounts[elem] > maxRepetitionsSoFar:
        elemWithMaxRepetitions = elem
        maxRepetitionsSoFar = mapWithElemCounts[elem]

print(f"elem with max repetitions: {elemWithMaxRepetitions}")
print(f"number of distinct elements: {len(mapWithElemCounts)}")
