import random

def procuctOfDigits(num):
    product = 1
    for digit in str(num):
        product *= int(digit)
    return product

def createDictionany(k, n, m):
    dictionary = {}
    for i in range(k):
        randomNum = random.randint(n, m)
        dictionary[randomNum] = procuctOfDigits(randomNum)

    return dictionary

dictionary = createDictionany(5, 1, 1000)

maximum = float('-inf')
minimum = float('inf')

for key in dictionary:
    minimum = min(minimum, dictionary[key])
    maximum = max(maximum, dictionary[key])

print(dictionary)
print(minimum)
print(maximum)
