def countOfDivisables(num):
    divisables = set([])
    for i in range(2, num):
        if num % i == 0:
            divisables.add(i)

    return divisables


for i in range(10, 1001):
    res = countOfDivisables(i)
    if len(res) == 2 and 11 in res:
        print(i)

